local status, _ = pcall(vim.cmd, "colorscheme tokyonight-moon")
if not status then
    print("Could not enable your colorscheme")
    return
end

