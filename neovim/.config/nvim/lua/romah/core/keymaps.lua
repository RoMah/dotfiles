-- Personal Keymaps for NeoVim

-- NeoVim Leader Mapping
vim.g.mapleader = " " -- Uses space for the Leader key


local keymap = vim.keymap


-- When not in Normal mode, Use 'jk' to return to Normal mode
keymap.set("i", "jk", "<ESC>")

-- When deleting a character, do not put it in the buffer
keymap.set("n", "x", '"_x')


-- Better Pane management
keymap.set("n", "<leader>sv", "<C-w>v") -- Split Vertically
keymap.set("n", "<leader>sh", "<C-w>s") -- Split Horizontally
keymap.set("n", "<leader>se", "<C-w>=") -- Split Equal Width
keymap.set("n", "<leader>sc", ":close<CR>") -- Split Equal Width

-- Tab Management
keymap.set("n", "<leader>to", ":tabnew<CR>") -- Open new tab 
keymap.set("n", "<leader>tc", ":tabclose<CR>") -- Close current tab
keymap.set("n", "<leader>tn", ":tabn<CR>") -- Go to the next tab
keymap.set("n", "<leader>tp", ":tabp<CR>") -- Go to the previous tab


-- Plugin Keymaps
keymap.set("n", "<leader>sm", ":MaximizerToggle<CR>") -- Toggle fullscreen splits
