-- #### Default NeoVim Settings ####

-- NeoVim GUI
vim.opt.guicursor = ""
vim.opt.errorbells = false 
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.cmdheight = 1 -- More space for displaying messages
vim.opt.colorcolumn = "80"
vim.opt.cursorline = true
vim.opt.cursorcolumn = true
vim.opt.clipboard:append("unnamedplus")
vim.opt.wildmode = longest,list

-- NeoVim Perf
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true
vim.opt.updatetime = 50


-- Line Numbering
vim.opt.number = true
vim.opt.relativenumber = true

-- Default Tab and Indentation Behaviour
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true -- Converts tabs to spaces
vim.opt.smartindent = true

-- Split Windows
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Line Rendering
vim.opt.wrap = false

-- Terminal GUI Colours
vim.opt.termguicolors = true

-- Search Options
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.incsearch = true

-- Editing Behaviours
vim.opt.iskeyword:append("-")
