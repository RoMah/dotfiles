-- Auto-Installs packer if not installed
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

-- Autocommand that reloads NeoVim and installs/updates/removes plugins
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost packer-setup.lua source <afile> | PackerSync
  augroup end
]])


-- Import packer safely
local status, packer = pcall(require, "packer")
if not status then
    print("Could not import Packer safely...Exiting.")
    return
end


return packer.startup(function(use)
  -- My plugins here
  use 'wbthomason/packer.nvim' -- Packer can manage itself
  use 'folke/tokyonight.nvim'

  use {
  'nvim-lualine/lualine.nvim',
  requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }


  use 'christoomey/vim-tmux-navigator'
  use 'szw/vim-maximizer'
  use 'tpope/vim-surround'
  




  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
