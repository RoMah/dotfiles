-- Import Comment plugin safely
local setup, comment = pcall(require, "Comment")
if not setup then
    return
end

-- Enable Comment functionality
comment.setup()
